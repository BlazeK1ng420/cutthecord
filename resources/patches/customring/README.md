## DisTok CutTheCord: Custom Ring Instructions

This patch is a simple file replacement, so it doesn't have patch files.

Replace the `res/raw/call_ringing.mp3` with any ring sound you want, such as [this masterpiece](https://ralsei-appreciation.club/i/m1gn.mp3) ([source](https://soundcloud.com/yahia-mice/sans_calling_discord)), and you're done.

